/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.mango.domain;

import java.util.Date;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class Shift extends AbstractEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PUMPIST")
    private User pumpist;
    
    @ElementCollection
    @MapKeyColumn(name = "FUEL_TYPE")
    @Column(name = "VALUE")
    @CollectionTable(name = "MECH_INDEX", joinColumns = @JoinColumn(name = "SHIFT"))
    private Map<FuelType, Index> mechIndex;
    
    @ElementCollection
    @MapKeyColumn(name = "FUEL_TYPE")
    @Column(name = "VALUE")
    @CollectionTable(name = "ELEC_INDEX", joinColumns = @JoinColumn(name = "SHIFT"))
    private Map<FuelType, Index> elecIndex;

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public User getPumpist() {
        return pumpist;
    }

    public void setPumpist(User pumpist) {
        this.pumpist = pumpist;
    }

    
    public Map<FuelType, Index> getMechIndex() {
        return mechIndex;
    }

    public void setMechIndex(Map<FuelType, Index> mechIndex) {
        this.mechIndex = mechIndex;
    }

    public Map<FuelType, Index> getElecIndex() {
        return elecIndex;
    }

    public void setElecIndex(Map<FuelType, Index> elecIndex) {
        this.elecIndex = elecIndex;
    }
    
    

}
