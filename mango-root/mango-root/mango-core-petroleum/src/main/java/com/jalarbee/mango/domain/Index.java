/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import javax.persistence.Embeddable;

/**
 *
 * @author abdoul
 */
@Embeddable
public class Index {
    
    private Double valueFrom;
    private Double valueTo;

    public Double getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(Double valueFrom) {
        this.valueFrom = valueFrom;
    }

    public Double getValueTo() {
        return valueTo;
    }

    public void setValueTo(Double valueTo) {
        this.valueTo = valueTo;
    }
    
    
}
