/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class Gauge extends AbstractEntity {
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date gaugeDate;
    private Double quantity;
    
    @ManyToOne
    private Tank tank;
    
    @PrePersist
    protected void replicate() {
        tank.setQuantity(quantity);
    }

    public Date getGaugeDate() {
        return gaugeDate;
    }

    public void setGaugeDate(Date gaugeDate) {
        this.gaugeDate = gaugeDate;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
    
    
}
