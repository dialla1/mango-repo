/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.Station;

/**
 *
 * @author abdoul
 */
public interface StationService {
    Station load(long stationId);
}
