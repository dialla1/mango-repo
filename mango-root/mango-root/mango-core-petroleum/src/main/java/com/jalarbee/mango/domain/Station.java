/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.mango.domain;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 *
 * @author abdoul
 */
@Entity
@DiscriminatorValue("G")
public class Station extends BusinessUnit {

    @OneToMany(mappedBy = "station")
    @OrderBy(value = "name")
    private List<Pump> pumps;

    @OneToMany
    @OrderBy(value = "name")
    private List<Tank> tanks;

    public List<Pump> getPumps() {
        return pumps;
    }

    public void setPumps(List<Pump> pumps) {
        this.pumps = pumps;
    }

    public List<Tank> getTanks() {
        return tanks;
    }

    public void setTanks(List<Tank> tanks) {
        this.tanks = tanks;
    }

}
