/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.repository;

import com.jalarbee.mango.domain.Station;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author abdoul
 */
public interface StationRepository extends PagingAndSortingRepository<Station, Long>{
    
//    @Query("SELECT DISTINCT s FROM Station s LEFT JOIN FETCH s.tanks LEFT JOIN FETCH s.pumps p LEFT JOIN FETCH p.shift WHERE s.id=:stationId")
//    public Station load(@Param("stationId") long stationId);
}
