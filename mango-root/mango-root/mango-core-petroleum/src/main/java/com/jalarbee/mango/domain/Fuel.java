/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author abdoul
 */
@Entity
public class Fuel extends Product {
    
    @Enumerated(EnumType.STRING)
    private FuelType type;

    public FuelType getType() {
        return type;
    }

    public void setType(FuelType type) {
        this.type = type;
    }
    
}
