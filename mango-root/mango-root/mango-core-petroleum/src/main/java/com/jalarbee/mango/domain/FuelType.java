/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.mango.domain;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author abdoul
 */
public enum FuelType {

    SUPER('S'), DIESEL('D');

    private static final Map<Character, FuelType> MAP = new HashMap(){{
        for(FuelType type : FuelType.values()) {
            put(type.getKey(), type);
        }
    }};

    private final Character key;

    FuelType(final Character key) {
        this.key = key;
    }

    public Character getKey() {
        return key;
    }

    public static FuelType getType(String dbVal) {
        return MAP.get(dbVal.charAt(0));
    }
}
