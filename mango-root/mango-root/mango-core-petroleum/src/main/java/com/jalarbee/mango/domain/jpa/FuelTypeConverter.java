/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain.jpa;

import com.jalarbee.mango.domain.FuelType;
import javax.persistence.AttributeConverter;

/**
 *
 * @author abdoul
 */
public class FuelTypeConverter implements AttributeConverter<FuelType, String> {

    @Override
    public String convertToDatabaseColumn(FuelType x) {
        return x.getKey().toString();
    }

    @Override
    public FuelType convertToEntityAttribute(String y) {
        return FuelType.getType(y);
    }
    
}
