package com.vaadin.demo.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MangoEcomApplication {

    public static void main(String[] args) {
        SpringApplication.run(MangoEcomApplication.class, args);
    }
}
