package com.jalarbee.mango.ecom;

import com.vaadin.demo.dashboard.MangoEcomApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MangoEcomApplication.class)
@WebAppConfiguration
public class MangoEcomApplicationTests {

	@Test
	public void contextLoads() {
	}

}
