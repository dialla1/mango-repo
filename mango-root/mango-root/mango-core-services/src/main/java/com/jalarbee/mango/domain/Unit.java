package com.jalarbee.mango.domain;

public enum Unit {

	DEF("DEFAULT", "unit.default"), KG("KG", "unit.kilogram"), T("TON", "unit.ton");
	
	private final String key;
	private final String notation;
	
	private Unit(String notation, String key) {
		this.key = key;
		this.notation = notation;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getNotation() {
		return notation;
	}
	
	@Override
	public String toString() {
		return this.name();
	}
}
