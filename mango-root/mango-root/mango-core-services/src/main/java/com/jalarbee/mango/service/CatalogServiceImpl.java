package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.Catalog;
import com.jalarbee.mango.domain.Category;
import com.jalarbee.mango.domain.Product;
import com.jalarbee.mango.domain.Sku;
import com.jalarbee.mango.domain.Status;
import com.jalarbee.mango.repository.CatalogRepository;
import com.jalarbee.mango.repository.CategoryRepository;
import com.jalarbee.mango.repository.ProductRepository;
import com.jalarbee.mango.repository.SkuRepository;
import com.jalarbee.mango.service.exception.NoSuchSkusFoundException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

@Service(value = "catalogService")
@Transactional(propagation = Propagation.MANDATORY)
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    private CatalogRepository catalogRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SkuRepository skuRepository;

    private static Logger logger = LoggerFactory.getLogger(CatalogServiceImpl.class);

    @Override
    public Catalog fetch(long businessUnitId) {
        return catalogRepository.findById(businessUnitId);
    }

    @Override
    public Product readProduct(long productId) {
        return findProduct(productId);
    }

    @Override
    public Product addSkus(long productId, List<Sku> options) throws SkuAlreadyExistsException {
        Product product = findProduct(productId);
        List<Sku> skus = product.getAdditionalSkus();
        List<Sku> existingOnes = new ArrayList<Sku>();
        List<Sku> addedOnes = new ArrayList<Sku>();
        for (Sku option : options) {
            if (!skus.contains(option) && !product.getDefaultSku().equals(option)) {
                logger.info("adding sku {}", option);
                skus.add(option);
                addedOnes.add(option);
            } else {
                logger.error("this sku {} already exists.", option);
                existingOnes.add(option);
            }
        }
        if (existingOnes.size() == 0) {
            return product;
        }
        throw new SkuAlreadyExistsException(product, existingOnes, addedOnes);
    }

    @Override
    public Product updateSkus(long productId, List<Sku> options) throws NoSuchSkusFoundException {
        Product product = findProduct(productId);
        List<Sku> skus = product.getAllSkus();
        List<Sku> notFound = new ArrayList<Sku>();
        for (Sku option : options) {
            boolean found = false;
            for (Sku sku : skus) {
                if (sku.getId() == option.getId()) {
                    logger.info("updating sku {} with value {}", sku, option);
                    sku = option;
                    found = true;
                }
            }
            if (!found) {
                notFound.add(option);
            }
        }
        if (notFound.isEmpty()) {
            return product;
        }
        throw new NoSuchSkusFoundException(notFound, product);
    }

    @Override
    public Product removeSkus(long productId, List<Sku> options) throws NoSuchSkusFoundException, DefaultSkuMandatoryException {
        Product product = findProduct(productId);
        List<Sku> skus = product.getAdditionalSkus();
        List<Sku> notFound = new ArrayList<>();
        options.stream().forEach((Sku option) -> {
            if (skus.contains(option)) {
                logger.info("removing sku {}", option);
                skus.remove(option);
            } else {
                notFound.add(option);
                logger.error("this sku {} does not exist.", option);
            }
        });
        if (options.contains(product.getDefaultSku())) {
            notFound.remove(product.getDefaultSku());
            if (!product.getAdditionalSkus().isEmpty()) {
                Sku newDefaultSku = product.getAdditionalSkus().get(0);
                logger.error("sku {} will be promoted to default sku", newDefaultSku);
                product.getAdditionalSkus().remove(0);
                product.setDefaultSku(newDefaultSku);
            } else {
                logger.error("there is no additional sku to promote to default sku.");
                throw new DefaultSkuMandatoryException(product);
            }
        }
        if (notFound.isEmpty()) {
            return product;
        }
        throw new NoSuchSkusFoundException(notFound, product);
    }

    private Product findProduct(long productId) {
        return productRepository.findOne(productId);
    }

    @Override
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product deleteProduct(Product product) {
        product.setStatus(Status.DELETED);
        return productRepository.save(product);
    }

    @Override
    public Iterable<Product> addProducts(List<Product> products) {
        return productRepository.save(products);
    }

    @Override
    public Iterable<Product> removeProducts(List<Product> products) {
        for (Product product : products) {
            product.setStatus(Status.DELETED);
        }
        return productRepository.save(products);
    }

    @Override
    public Iterable<Product> updateProducts(List<Product> products) {
        return productRepository.save(products);
    }

    @Override
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category updateCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category removeCategory(Category category) {
        category.setStatus(Status.DELETED);
        return categoryRepository.save(category);
    }

    @Override
    public Category readCategory(long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    @Override
    public Sku findSku(long skuId) {
        return skuRepository.findOne(skuId);
    }

}
