package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.Catalog;
import com.jalarbee.mango.domain.Category;
import com.jalarbee.mango.domain.Product;
import com.jalarbee.mango.domain.Sku;
import com.jalarbee.mango.service.exception.NoSuchSkusFoundException;
import java.util.List;

public interface CatalogService {

    Catalog fetch(long businessUnitId);

    Product addSkus(long productId, List<Sku> skus) throws SkuAlreadyExistsException;

    Product removeSkus(long productId, List<Sku> skus) throws NoSuchSkusFoundException, DefaultSkuMandatoryException;

    Product updateSkus(long productId, List<Sku> skus) throws NoSuchSkusFoundException;

    Product readProduct(long productId);

    Product addProduct(Product product);

    Product updateProduct(Product product);

    Product deleteProduct(Product product);

    Iterable<Product> addProducts(List<Product> products);

    Iterable<Product> removeProducts(List<Product> products);

    Iterable<Product> updateProducts(List<Product> products);

    Category addCategory(Category category);

    Category updateCategory(Category category);

    Category removeCategory(Category category);

    Category readCategory(long categoryId);

    public Sku findSku(long skuId);

}
