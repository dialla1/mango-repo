package com.jalarbee.mango.domain;

import java.util.HashMap;
import java.util.Map;

public class Status {

    private static final Map<String, Status> MAP = new HashMap<>();

    public static final Status NULL = new Status("");
    public static final Status ACTIVE = new Status("A");
    public static final Status DELETED = new Status("D");

    private final String notation;

    public Status(final String notation) {
        this.notation = notation;
        MAP.put(notation, this);
    }

    public String getNotation() {
        return notation;
    }

    public static Status getStatus(final String status) {
        return MAP.get(status);
    }

}
