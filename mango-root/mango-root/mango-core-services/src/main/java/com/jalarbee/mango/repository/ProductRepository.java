package com.jalarbee.mango.repository;


import org.springframework.data.repository.PagingAndSortingRepository;

import com.jalarbee.mango.domain.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

}
