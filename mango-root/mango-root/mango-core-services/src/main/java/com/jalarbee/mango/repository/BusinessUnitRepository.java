package com.jalarbee.mango.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.jalarbee.mango.domain.BusinessUnit;

public interface BusinessUnitRepository extends PagingAndSortingRepository<BusinessUnit, Long> {

}
