/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain.jpa;

import com.jalarbee.mango.domain.EventType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abdoul
 */
@Converter(autoApply = true)
public class EventTypeConverter implements AttributeConverter<EventType, String>{

    @Override
    public String convertToDatabaseColumn(EventType x) {
        return x.getNotation();
    }

    @Override
    public EventType convertToEntityAttribute(String y) {
        return EventType.getEventType(y);
    }

    
}
