package com.jalarbee.mango.domain;

import java.math.BigDecimal;

public interface Priceable {

    public BigDecimal getRetailPrice();

    public BigDecimal getBulkRetailPrice();

    public BigDecimal getStorePrice();
}
