package com.jalarbee.mango.domain;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "UZER")
public class User extends Account {

    private static final long serialVersionUID = 1L;

    private String firstName;
    private String lastName;

    private String businessName;
    
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="RECIPIENT_ID", referencedColumnName="ID")
    private List<User> recipients;
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public List<User> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<User> recipients) {
        this.recipients = recipients;
    }
    
    @Override
    public String toString() {
        return (accountType == AccountType.PHYSIC) ? 
        String.format("User[id=%d, firstName='%s', lastName='%s']", super.getId(), firstName, lastName) :
        String.format("User[id=%d, businessName='%s']", super.getId(), businessName);
    }
}
