package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findOne(long userId);
}
