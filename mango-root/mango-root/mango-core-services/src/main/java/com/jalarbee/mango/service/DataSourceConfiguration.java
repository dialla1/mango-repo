/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import javax.sql.DataSource;

/**
 *
 * @author abdoul
 */
public interface DataSourceConfiguration {
    DataSource dataSource();
}
