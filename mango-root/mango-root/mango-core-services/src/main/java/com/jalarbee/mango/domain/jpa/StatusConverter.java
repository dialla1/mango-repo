package com.jalarbee.mango.domain.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.jalarbee.mango.domain.Status;

@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, String> {

    @Override
    public String convertToDatabaseColumn(Status attribute) {
        return attribute.getNotation();
    }

    @Override
    public Status convertToEntityAttribute(String dbData) {
        return Status.getStatus(dbData);
    }

}
