package com.jalarbee.mango.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address implements Serializable {

    @Column(name="ADDRESS_LINE1")
    private String line1;
    @Column(name="ADDRESS_LINE2")
    private String line2;
    @Column(name="ADDRESS_CITY")
    private String city;
    @Column(name="ADDRESS_ZIP_CODE")
    private String zipCode;
    @Column(name="ADDRESS_AREA_CODE")
    private String areaCode;
    @Column(name="ADDRESS_COUNTRY")
    private String country;

    public Address() {
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
