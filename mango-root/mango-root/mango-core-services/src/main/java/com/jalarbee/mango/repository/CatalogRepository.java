package com.jalarbee.mango.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.jalarbee.mango.domain.Catalog;

public interface CatalogRepository extends PagingAndSortingRepository<Catalog, Long> {

    Catalog findById(long businessUnitId);
}
