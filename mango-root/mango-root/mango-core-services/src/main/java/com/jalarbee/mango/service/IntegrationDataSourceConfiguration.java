package com.jalarbee.mango.service;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;


@Profile("integration")
@Configuration
public class IntegrationDataSourceConfiguration implements DataSourceConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(IntegrationDataSourceConfiguration.class);
	
	@Bean
        @Override
	public DataSource dataSource() {
		logger.info("creating embedded datasource for integration testing");
		return new EmbeddedDatabaseBuilder().setName("DXSCH").setType(EmbeddedDatabaseType.H2)
				.addScript("classpath:schema.sql")
				.addScript("classpath:data.sql").build();
	}

}
