/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author abdoul
 */

@Entity
public class LineItem extends AbstractEntity {
    
    @OneToOne
    @JoinColumn(name = "SKU")
    private Sku sku;
    
    @Basic
    private double quantity;
    
    @Basic
    private String description;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "CREDIT_ORDER")
    private CreditOder creditOder;

    public Sku getSku() {
        return sku;
    }

    public void setSku(Sku sku) {
        this.sku = sku;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public CreditOder getCreditOder() {
        return creditOder;
    }

    public void setCreditOder(CreditOder creditOder) {
        this.creditOder = creditOder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
        
}
