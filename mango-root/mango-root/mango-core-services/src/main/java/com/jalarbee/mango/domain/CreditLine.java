/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class CreditLine extends AbstractEntity {

    @OneToOne(optional = false)
    @JoinColumn(name = "CLIENT")
    private User client;

    public CreditLine() {
        Date date = new Date();
        this.status = Status.ACTIVE;
        this.openDate = date;
        this.lastActivityTime = date;
    }

    public CreditLine(User client) {
        this();
        this.client = client;
    }
    
    @Basic
    private BigDecimal credLimit = BigDecimal.ZERO;
    
    @OneToMany(mappedBy = "creditLine",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<CreditOder> orders = new HashSet<>();
    
    @OneToOne(cascade = CascadeType.PERSIST)
    @MapsId
    private CreditTimeline creditTimeline;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date openDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastActivityTime;
    
    private Status status;

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public BigDecimal getCredLimit() {
        return credLimit;
    }

    public void setCredLimit(BigDecimal credLimit) {
        this.credLimit = credLimit;
    }

    public Set<CreditOder> getOrders() {
        return orders;
    }

    public void setOrders(Set<CreditOder> orders) {
        this.orders = orders;
    }

    public CreditTimeline getCreditTimeline() {
        return creditTimeline;
    }

    public void setCreditTimeline(CreditTimeline creditTimeline) {
        this.creditTimeline = creditTimeline;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    
}

