package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.BusinessUnit;
import com.jalarbee.mango.domain.Organization;
import com.jalarbee.mango.domain.Status;
import com.jalarbee.mango.repository.BusinessUnitRepository;
import com.jalarbee.mango.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "organizationService")
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private BusinessUnitRepository businessUnitRepository;

    @Override
    public Organization readOrganization(long organizationId) {
        return organizationRepository.findOne(organizationId);
    }

    @Override
    public Organization addOrganization(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public Organization updateOrganization(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    public Organization deleteOrganization(long organizationId) {
        Organization organization = organizationRepository.findOne(organizationId);
        organization.setStatus(Status.DELETED);
        return organization;
    }

    @Override
    public Iterable<Organization> list() {
        return organizationRepository.findAll();
    }

    @Override
    public Iterable<BusinessUnit> getBusninessUnits(long organizationId) {
        return businessUnitRepository.findAll();
    }

    @Override
    public BusinessUnit readBusinessUnit(long businessUnitId) {
        return businessUnitRepository.findOne(businessUnitId);
    }

    @Override
    public BusinessUnit addBusinessUnit(BusinessUnit businessUnit) {
        return businessUnitRepository.save(businessUnit);
    }

    @Override
    public BusinessUnit deleteBusinessUnit(BusinessUnit businessUnit) {
        businessUnit.setStatus(Status.DELETED);
        return businessUnit;
    }

    @Override
    public BusinessUnit updateBusinessUnit(BusinessUnit businessUnit) {
        return businessUnitRepository.save(businessUnit);
    }

}
