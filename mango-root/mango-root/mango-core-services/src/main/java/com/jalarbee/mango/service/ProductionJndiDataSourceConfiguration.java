package com.jalarbee.mango.service;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;



@Profile("production")
@Configuration
public class ProductionJndiDataSourceConfiguration implements DataSourceConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(ProductionJndiDataSourceConfiguration.class);

    @Resource(mappedName = "jdbc/datasource")
    private DataSource dataSource;

    @Bean
    @Override
    public DataSource dataSource() {
        logger.info("datasource created is {}", dataSource);
        return dataSource;
    }

}
