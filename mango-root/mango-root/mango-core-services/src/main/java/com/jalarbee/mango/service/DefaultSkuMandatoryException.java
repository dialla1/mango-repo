package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.Product;

public class DefaultSkuMandatoryException extends Throwable {

    private static final long serialVersionUID = 1L;
    private final Product product;

    public DefaultSkuMandatoryException(Product product) {
        super();
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }
}
