package com.jalarbee.mango.domain;

import java.util.HashMap;
import java.util.Map;

public enum SkuOption {

    NULL("option.null", null),
    UNIT_QUANTITY("option.unit.quantity", "UQ"),
    SIZE("option.size", "SZ"),
    COLOR("option.color", "CL"),
    OTHER("option.other", "OT");

    private static final Map<String, SkuOption> CONVERTER_MAP = new HashMap<String, SkuOption>() {
        private static final long serialVersionUID = 1L;

        {
            for (final SkuOption option : SkuOption.values()) {
                put(option.getName(), option);
            }
        }
    };

    private final String name;
    private final String key;

    private SkuOption(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public static SkuOption getOption(String name) {
        return CONVERTER_MAP.get(name);
    }

}
