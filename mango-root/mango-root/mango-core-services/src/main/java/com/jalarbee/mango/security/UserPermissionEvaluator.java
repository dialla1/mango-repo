package com.jalarbee.mango.security;

import com.jalarbee.mango.domain.Role;
import com.jalarbee.mango.domain.User;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class UserPermissionEvaluator /*implements PermissionEvaluator*/ {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserPermissionEvaluator.class);

//    @Override
//    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
//        LOGGER.debug("checking permissions for {} on target {} and permission {} ", new Object[]{authentication, targetDomainObject, permission});
//        if (authentication == null) {
//            return false;
//        }
//        if (targetDomainObject instanceof User) {
//            User user = (User) targetDomainObject;
//            User currentUser = (User) authentication.getPrincipal();
//            if (currentUser.hasRole(Role.ADMIN) && user.getOrganization().getId() == currentUser.getOrganization().getId()) {
//                return true;
//            }
//        }
//        return false;
//    }

//    @Override
//    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
//        throw new UnsupportedOperationException();
//    }

}
