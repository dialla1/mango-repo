package com.jalarbee.mango.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.jalarbee.mango.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findByUserName(String username);

    List<User> findUsersByFirstNameOrLastNameOrUserName(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("userName") String userName);
}
