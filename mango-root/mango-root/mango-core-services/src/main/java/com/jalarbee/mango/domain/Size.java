package com.jalarbee.mango.domain;

public enum Size {

    DEF("DEFAULT", "size.default"), S("S", "size.small"), M("M", "size.medium"), L("L", "size.large"), XL("XL", "size.extra.large"), XXL("XXL", "size.double.extra.large");

    private final String notation;
    private final String key;

    private Size(String notation, String key) {
        this.key = key;
        this.notation = notation;
    }

    public String getKey() {
        return key;
    }

    public String getNotation() {
        return notation;
    }

    @Override
    public String toString() {
        return this.name();
    }

}
