package com.jalarbee.mango.service;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Profile("test")
@Configuration
public class TestDataSourceConfiguration implements DataSourceConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(TestDataSourceConfiguration.class);

    @Bean
    @Override
    public DataSource dataSource() {
        logger.info("creating embedded datasource for unit testing");
        return new EmbeddedDatabaseBuilder().setName("DXSCH").setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:database/schema.sql")
                .addScript("classpath:database/test-data.sql").build();
    }

}
