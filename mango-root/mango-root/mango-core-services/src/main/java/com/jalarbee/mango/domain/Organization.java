package com.jalarbee.mango.domain;

import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Organization extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String name;
    private String description;
    private Address address;

    @OneToMany(mappedBy = "organization")
    @OrderBy(value = "name asc")
    private Set<BusinessUnit> businessUnits = new HashSet<>();

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    private Currency currency;
    private Locale locale;

    private Status status;

    public Organization() {
        this.creationDate = new Date();
    }

    public Organization(Organization o) {
        this(o, new HashSet<>());
    }

    public Organization(Organization o, Set<BusinessUnit> units) {
        this();
        this.businessUnits = units;
        this.id = o.getId();
        this.name = o.getName();
        this.address = o.getAddress();
        this.description = o.getDescription();
        this.currency = o.getCurrency();
        this.locale = o.getLocale();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<BusinessUnit> getBusinessUnits() {
        return businessUnits;
    }

    public void setBusinessUnits(Set<BusinessUnit> businessUnits) {
        this.businessUnits = businessUnits;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Organization other = (Organization) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
