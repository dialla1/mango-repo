package com.jalarbee.mango.domain;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class Quantity {

    private final double number;
    @Enumerated(EnumType.STRING)
    private final Unit unit;

    public Quantity() {
        this.number = 1.0;
        this.unit = Unit.DEF;
    }

    public Quantity(double number, Unit unit) {
        this.number = number;
        this.unit = unit;
    }

    public double getNumber() {
        return number;
    }

    public Unit getUnit() {
        return unit;
    }

}
