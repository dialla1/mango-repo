/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.CreditLine;
import com.jalarbee.mango.domain.CreditOder;
import com.jalarbee.mango.repository.CreditLineRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abdoul
 */
@Service(value = "creditService")
@Transactional(Transactional.TxType.MANDATORY)
public class CreditServiceImpl implements CreditService {

    @Autowired private CreditLineRepository creditLineRepository;
    
    @Override
    public CreditLine create(CreditLine entity) {
        return creditLineRepository.save(entity);
    }

    @Override
    public CreditLine addCreditOrder(long creditLineId, CreditOder creditOder) {
        CreditLine creditLine = creditLineRepository.findOne(creditLineId);
        creditLine.getOrders().add(creditOder);
        return creditLine;
    }
    
}
