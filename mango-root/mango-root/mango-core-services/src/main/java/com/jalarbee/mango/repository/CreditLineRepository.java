/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.repository;

import com.jalarbee.mango.domain.CreditLine;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author abdoul
 */
public interface CreditLineRepository extends  PagingAndSortingRepository<CreditLine, Long> {
    
}
