/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author abdoul
 */
public enum AccountType {
    
    PHYSIC("P"), MORAL("M");
    
    private static final Map<String, AccountType> MAP = new HashMap<String, AccountType>() {{
        put(PHYSIC.getNotation(), PHYSIC);
        put(MORAL.getNotation(), MORAL);
    }} ;

    public static AccountType getAccountType(String y) {
        return MAP.get(y);
    }
    
    private final String notation;

    private AccountType(String notation) {
        this.notation = notation;
        
    }

    public String getNotation() {
        return notation;
    }
    
}
