package com.jalarbee.mango.domain;

import java.util.Comparator;

public class RoleComparator implements Comparator<Role> {

    @Override
    public int compare(Role arg0, Role arg1) {
        return arg0.getName().compareTo(arg1.getName());
    }

}
