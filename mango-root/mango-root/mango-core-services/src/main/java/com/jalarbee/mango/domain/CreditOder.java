/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class CreditOder extends AbstractEntity {
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;
    
    @OneToMany(mappedBy = "creditOder", cascade = CascadeType.PERSIST)
    private List<LineItem> productItems;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREDIT_LINE")
    private CreditLine creditLine;

    public CreditOder() {
        this(new Date(), new ArrayList<>(), null);
    }

    public CreditOder(Date orderDate, List<LineItem> productItems, CreditLine creditLine) {
        this.orderDate = orderDate;
        this.productItems = productItems;
        this.creditLine = creditLine;
    }
    
    public CreditOder(Date orderDate, CreditLine creditLine) {
        this(orderDate, new ArrayList<>(), creditLine);
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<LineItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<LineItem> productItems) {
        this.productItems = productItems;
    }

    public CreditLine getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(CreditLine creditLine) {
        this.creditLine = creditLine;
    }
    
    @PostPersist
    public void onPersist() {
        TimelineEvent event;
        for(LineItem item: productItems) {
            event = new TimelineEvent(orderDate, item.getDescription(), EventType.CREDIT, new CreditSummary(), creditLine.getCreditTimeline());
            creditLine.getCreditTimeline().getEvents().add(event);
        }
        
    }
}
