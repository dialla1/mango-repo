package com.jalarbee.mango.domain;

import java.math.BigDecimal;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class PricedItem extends AbstractEntity {

    private BigDecimal bulkRetailPrice;
    private BigDecimal retailPrice;
    private BigDecimal creditPrice;
    private BigDecimal storePrice;

    public BigDecimal getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(BigDecimal retailPrice) {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getCreditPrice() {
        return creditPrice;
    }

    public void setCreditPrice(BigDecimal creditPrice) {
        this.creditPrice = creditPrice;
    }

    public BigDecimal getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(BigDecimal storePrice) {
        this.storePrice = storePrice;
    }

    public BigDecimal getBulkRetailPrice() {
        return bulkRetailPrice;
    }

    public void setBulkRetailPrice(BigDecimal bulkRetailPrice) {
        this.bulkRetailPrice = bulkRetailPrice;
    }

}
