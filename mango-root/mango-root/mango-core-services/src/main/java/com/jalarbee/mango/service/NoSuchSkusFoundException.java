package com.jalarbee.mango.service.exception;

import com.jalarbee.mango.domain.Product;
import com.jalarbee.mango.domain.Sku;
import java.util.List;

public class NoSuchSkusFoundException extends Throwable {

    private static final long serialVersionUID = 1L;
    private final List<Sku> notFoundSkus;
    private final Product product;

    public NoSuchSkusFoundException(List<Sku> notFoundSkus, Product product) {
        super();
        this.notFoundSkus = notFoundSkus;
        this.product = product;
    }

    public List<Sku> getNotFoundSkus() {
        return notFoundSkus;
    }

    public Product getProduct() {
        return product;
    }
}
