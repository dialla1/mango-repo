/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class CreditTimeline extends AbstractEntity {
    
    private CreditSummary creditSummary;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastActivityTime;
    
    @OneToMany(mappedBy = "creditTimeline", cascade = CascadeType.MERGE)
    private List<TimelineEvent> events;

    public CreditTimeline() {
        this.lastActivityTime = new Date();
        this.events = new ArrayList<>();
    }

    
    
    public List<TimelineEvent> getEvents() {
        return events;
    }

    public void setEvents(List<TimelineEvent> events) {
        this.events = events;
    }

    public CreditSummary getCreditSummary() {
        return creditSummary;
    }

    public void setCreditSummary(CreditSummary creditSummary) {
        this.creditSummary = creditSummary;
    }

    public Date getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }
    
    
}
