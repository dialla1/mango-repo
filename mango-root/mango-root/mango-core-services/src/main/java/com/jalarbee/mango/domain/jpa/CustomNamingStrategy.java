/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.mango.domain.jpa;

import org.hibernate.annotations.common.util.StringHelper;
import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 *
 * @author abdoul
 */
public class CustomNamingStrategy extends ImprovedNamingStrategy {

    @Override
    public String classToTableName(String className) {
        return tableName(StringHelper.unqualify(className));
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        return addUnderscores(propertyName).toUpperCase();
    }

    @Override
    public String tableName(String tableName) {
        return addUnderscores(tableName).toUpperCase();
    }

    @Override
    public String columnName(String columnName) {
        return addUnderscores(columnName).toUpperCase();
    }

    public String propertyToTableName(String className, String propertyName) {
        return classToTableName(className) + '_' + propertyToColumnName(propertyName);
    }

    @Override
    public String foreignKeyColumnName(String propertyName, String propertyEntityName, String propertyTableName, String referencedColumnName) {
        return addUnderscores(propertyName).toUpperCase();
    }

    @Override
    public String joinKeyColumnName(String joinedColumn, String joinedTable) {
        return addUnderscores(joinedColumn).toUpperCase();
    }

}
