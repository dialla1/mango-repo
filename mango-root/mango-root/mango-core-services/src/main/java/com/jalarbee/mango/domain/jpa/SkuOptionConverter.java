package com.jalarbee.mango.domain.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.jalarbee.mango.domain.SkuOption;

@Converter(autoApply = true)
public class SkuOptionConverter implements AttributeConverter<SkuOption, String> {

    @Override
    public String convertToDatabaseColumn(SkuOption attribute) {
        return attribute.getName();
    }

    @Override
    public SkuOption convertToEntityAttribute(String dbData) {
        return SkuOption.getOption(dbData);
    }

}
