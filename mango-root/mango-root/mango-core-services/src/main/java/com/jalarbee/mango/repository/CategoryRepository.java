package com.jalarbee.mango.repository;

import org.springframework.data.repository.CrudRepository;

import com.jalarbee.mango.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
