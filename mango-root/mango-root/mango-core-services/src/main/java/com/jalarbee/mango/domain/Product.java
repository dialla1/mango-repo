package com.jalarbee.mango.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.util.CollectionUtils;

@Entity
public class Product extends PricedItem {

    private String name;
    private String description;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CATEGORY")
    private Category category;

    @OneToMany(mappedBy = "product")
    private List<Sku> additionalSkus;

    @OneToOne(mappedBy = "defaultProduct")
    private Sku defaultSku;

    private Status status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public List<Sku> getAdditionalSkus() {
        return additionalSkus;
    }

    public void setAdditionalSkus(List<Sku> additionalSkus) {
        this.additionalSkus = additionalSkus;
    }

    public Sku getDefaultSku() {
        return defaultSku;
    }

    public void setDefaultSku(Sku defaultSku) {
        this.defaultSku = defaultSku;
    }

    public List<Sku> getAllSkus() {
        if (CollectionUtils.isEmpty(additionalSkus)) {
            return Arrays.asList(defaultSku);
        }
        return merge(defaultSku, additionalSkus);
    }

    private List<Sku> merge(Sku def, List<Sku> list) {
        List<Sku> skus = new ArrayList<Sku>();
        skus.add(def);
        for (Sku sku : list) {
            if (def.getId() != sku.getId()) {
                skus.add(sku);
            }
        }
        return skus;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((category == null) ? 0 : category.hashCode());
        result = prime * result
                + ((defaultSku == null) ? 0 : defaultSku.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Product other = (Product) obj;
        if (category == null) {
            if (other.category != null) {
                return false;
            }
        } else if (!category.equals(other.category)) {
            return false;
        }
        if (defaultSku == null) {
            if (other.defaultSku != null) {
                return false;
            }
        } else if (!defaultSku.equals(other.defaultSku)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return category.getName() + "/" + name;
    }

}
