/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import javax.persistence.Entity;

/**
 *
 * @author abdoul
 */

@Entity
public class Role extends AbstractEntity {
    
    private String name;
    private String notation;

    public Role() {
    }

    public Role(String name, String notation) {
        this.name = name;
        this.notation = notation;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotation() {
        return notation;
    }

    public void setNotation(String notation) {
        this.notation = notation;
    }
    
}
