/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author abdoul
 */
public enum EventType {
    
    CREDIT("C"), DEBIT("D");
    
    private static final Map<String, EventType> MAP = new HashMap<String, EventType>() {{
        put(CREDIT.getNotation(), CREDIT);
        put(DEBIT.getNotation(), DEBIT);
    }} ;
    
    private final String notation;

    private EventType(String notation) {
        this.notation = notation;
    }

    public String getNotation() {
        return notation;
    }
    
    public static EventType getEventType(String notation) {
        return MAP.get(notation);
    }
    
}
