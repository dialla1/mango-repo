package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.BusinessUnit;
import com.jalarbee.mango.domain.Organization;

public interface OrganizationService {

    Organization readOrganization(long organizationId);

    Organization addOrganization(Organization organization);

    Organization updateOrganization(Organization organization);

    Organization deleteOrganization(long organizationId);

    Iterable<Organization> list();

    Iterable<BusinessUnit> getBusninessUnits(long organizationId);

    BusinessUnit readBusinessUnit(long businessUnitId);

    BusinessUnit addBusinessUnit(BusinessUnit businessUnit);

    BusinessUnit deleteBusinessUnit(BusinessUnit businessUnit);

    BusinessUnit updateBusinessUnit(BusinessUnit businessUnit);
}
