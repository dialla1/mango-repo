/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.User;
import com.jalarbee.mango.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    private final UserRepository userRepository;

    @Autowired
    public CustomerServiceImpl(UserRepository customerRepository) {
        this.userRepository = customerRepository;
    }
    
    
    @Override
    public User find(long customer) {
        return userRepository.findOne(customer);
    }

    @Override
    public Iterable<User> getCutsomers() {
        return userRepository.findAll();
    }
    
}
