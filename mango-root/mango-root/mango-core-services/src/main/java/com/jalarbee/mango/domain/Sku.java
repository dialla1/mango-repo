package com.jalarbee.mango.domain;

import java.util.EnumMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;

@Entity
public class Sku extends PricedItem {

    private String name;
    private String description;

    @ElementCollection
    @MapKeyColumn(name = "OPTION")
    @Column(name = "VALUE")
    @CollectionTable(name = "SKU_OPTIONS", joinColumns = @JoinColumn(name = "SKU_ID"))
    private Map<SkuOption, String> options = new EnumMap<>(SkuOption.class); // maps from attribute name to value

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT", nullable = false)
    private Product product;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "DEF_PRODUCT", nullable = false)
    private Product defaultProduct;

    private Status status;

    public Sku() {
    }

    public Sku(final Product product) {
        assert product != null;
        this.product = product;
        this.name = product.getName();
        this.description = product.getDescription();
        this.status = Status.NULL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getDefaultProduct() {
        return defaultProduct;
    }

    public void setDefaultProduct(Product defaultProduct) {
        this.defaultProduct = defaultProduct;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<SkuOption, String> getOptions() {
        return options;
    }

    public void setOptions(Map<SkuOption, String> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "name: " + name + ", options: " + options;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((options == null || options.values() == null) ? 0 : options.values().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Sku other = (Sku) obj;
        if (options == null) {
            if (other.options != null) {
                return false;
            }
        } else if (!options.equals(other.options)) {
            return false;
        }
        return true;
    }

}
