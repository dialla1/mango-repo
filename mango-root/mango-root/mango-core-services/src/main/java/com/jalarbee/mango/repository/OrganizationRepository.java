package com.jalarbee.mango.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.jalarbee.mango.domain.Organization;

public interface OrganizationRepository extends PagingAndSortingRepository<Organization, Long> {

}
