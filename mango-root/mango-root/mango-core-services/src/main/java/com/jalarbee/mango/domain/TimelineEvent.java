/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain;

import java.util.Date;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author abdoul
 */
@Entity
public class TimelineEvent extends AbstractEntity {
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;
    
    private String eventName;
    
    @Enumerated(EnumType.STRING)
    private EventType eventType;
    
    @Embedded 
    private CreditSummary creditSummary;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREDIT_TIME_LINE")
    private CreditTimeline creditTimeline;

    public TimelineEvent() {
    }
    
    public TimelineEvent(Date eventTime, String eventName, EventType eventType, CreditSummary creditSummary, CreditTimeline creditTimeline) {
        this();
        this.eventTime = eventTime;
        this.eventName = eventName;
        this.eventType = eventType;
        this.creditSummary = creditSummary;
        this.creditTimeline = creditTimeline;
    }
    
    

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public CreditSummary getCreditSummary() {
        return creditSummary;
    }

    public void setCreditSummary(CreditSummary creditSummary) {
        this.creditSummary = creditSummary;
    }

    public CreditTimeline getCreditTimeline() {
        return creditTimeline;
    }

    public void setCreditTimeline(CreditTimeline creditTimeline) {
        this.creditTimeline = creditTimeline;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    
}
