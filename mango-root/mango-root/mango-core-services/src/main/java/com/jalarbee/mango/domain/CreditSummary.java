/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jalarbee.mango.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abdoul
 */
@Embeddable
public class CreditSummary implements Serializable {

    @Column(name = "SUMM_CREDIT")
    private BigDecimal credit;
    @Column(name = "SUMM_PAID")
    private BigDecimal paid;
    @Column(name = "SUMM_BALANCE")
    private BigDecimal balance;
    @Column(name = "SUMM_PROFIT")
    private BigDecimal profit;

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }
    
}
