/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.domain.jpa;

import com.jalarbee.mango.domain.AccountType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author abdoul
 */
@Converter(autoApply = true)
public class AccountTypeConverter implements AttributeConverter<AccountType, String>{

    @Override
    public String convertToDatabaseColumn(AccountType x) {
        return x.getNotation();
    }

    @Override
    public AccountType convertToEntityAttribute(String y) {
        return AccountType.getAccountType(y);
    }
    
}
