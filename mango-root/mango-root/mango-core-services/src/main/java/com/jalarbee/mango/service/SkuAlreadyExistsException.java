package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.Product;
import com.jalarbee.mango.domain.Sku;
import java.util.List;

public class SkuAlreadyExistsException extends Throwable {

    private static final long serialVersionUID = 1L;

    private final Product product;
    private final List<Sku> existingOnes;
    private final List<Sku> addedOnes;

    public SkuAlreadyExistsException(Product product, List<Sku> existingOnes, List<Sku> addedOnes) {
        super();
        this.product = product;
        this.existingOnes = existingOnes;
        this.addedOnes = addedOnes;
    }

    public Product getProduct() {
        return product;
    }

    public List<Sku> getExistingOnes() {
        return existingOnes;
    }

    public List<Sku> getAddedOnes() {
        return addedOnes;
    }

}
