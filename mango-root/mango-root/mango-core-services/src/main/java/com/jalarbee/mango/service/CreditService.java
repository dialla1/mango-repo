/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.CreditLine;
import com.jalarbee.mango.domain.CreditOder;

/**
 *
 * @author abdoul
 */

public interface CreditService {
    
    CreditLine create(CreditLine entity);
    CreditLine addCreditOrder(long creditLineId, CreditOder creditOder);
}
