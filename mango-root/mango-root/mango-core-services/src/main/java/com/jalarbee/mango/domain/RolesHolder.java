package com.jalarbee.mango.domain;

import java.util.Set;
import java.util.stream.Collectors;

public class RolesHolder {

    private final Set<Role> roles;
    private final Set<String> roleNames;

    public RolesHolder(Set<Role> roles) {
        this.roles = roles;
        roleNames = roles.stream().map(r -> r.getName()).collect(Collectors.toSet());
    }
    
    public boolean hasRole(Role role) {
        return roles.contains(role);
    }
    
    public boolean hasRole(String role) {
        return roleNames.contains(role);
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Set<String> getRoleNames() {
        return roleNames;
    }
    
}
