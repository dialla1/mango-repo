insert into DXSCH.ORGANIZATION (ID, NAME, DESCRIPTION, CREATION_DATE, ADDRESS_LINE1, ADDRESS_LINE2, ADDRESS_CITY, STATUS)  values (1, 'Namata SARL', 'the next big thing in this world!', CURRENT_TIMESTAMP(), 'Esccale', 'Marche Central' , 'KOLDA', 'A');

insert into DXSCH.BUSINESS_UNIT (ID, NAME, DESCRIPTION, CREATION_DATE, ORGANIZATION, ADDRESS_LINE1, ADDRESS_LINE2, ADDRESS_CITY, CATALOG, STATUS, BIZ_TYPE) values (1, 'Oilibya Kolda', 'the bets station service in town!', CURRENT_TIMESTAMP(), '1', 'Esccale', 'Marche Central' , 'KOLDA', '1', 'A', 'G');

insert into DXSCH.CATALOG (ID, NAME, DESCRIPTION, CREATION_DATE, LAST_UPDATE_TIME, STATUS) values (1, 'My Awesome Catalog', 'I am selling anything you buying', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'A');

insert into DXSCH.CATEGORY (ID, NAME, DESCRIPTION, CREATION_DATE, B_UNIT, CATALOG, STATUS) values(1, 'Boissons Sucres', 'rafraichissements, parties', CURRENT_TIMESTAMP(), '1', '1', 'A');
insert into DXSCH.CATEGORY (ID, NAME, DESCRIPTION, CREATION_DATE, B_UNIT, CATALOG, STATUS) values(2, 'Condiments', 'na saf sap!', CURRENT_TIMESTAMP(), '1', '1', 'A');
insert into DXSCH.CATEGORY (ID, NAME, DESCRIPTION, CREATION_DATE, B_UNIT, CATALOG, STATUS) values(3, 'Produits Laitiers', 'petits dejeuners', CURRENT_TIMESTAMP(), '1', '1', 'A');
insert into DXSCH.CATEGORY (ID, NAME, DESCRIPTION, CREATION_DATE, B_UNIT, CATALOG, STATUS) values(4, 'Viandes', 'texas bbq', CURRENT_TIMESTAMP(), '1', '1', 'A');

insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('1', 'Coca Cola 6', 'ciasse de 6 bouteilles', null, '1', 'A', '2350.00', '2200.00', '2150.00', '2400.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('2', 'Coca Cola 12', 'ciasse de 12 bouteilles', '1', null, 'A', '4500.00', '4300.00', '4200.00', '4800.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('3', 'Coca Cola 24', 'ciasse de 24 bouteilles', '1', null, 'A', '9000.00', '8900.00', '8800.00', '9500.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('4', 'Fanta', 'ciasse de 12 bouteilles', null, '2', 'A', '9000.00', '8900.00', '8800.00', '9500.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('5', 'Vimto', 'ciasse de 12 bouteilles', null, '3', 'A', '9000.00', '8900.00', '8800.00', '9500.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('6', 'Jumbo', 'sachet de 25 cubes', null, '4', 'A', '9000.00', '8900.00', '8800.00', '9500.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('7', 'La Vache Qui Rit', null, null, '5', 'A', '9000.00', '8900.00', '8800.00', '9500.00');
insert into DXSCH.SKU (ID, NAME, DESCRIPTION, PRODUCT, DEF_PRODUCT, STATUS, RETAIL_PRICE, BULK_RETAIL_PRICE, STORE_PRICE, CREDIT_PRICE) values ('8', 'Poulet Entier Grille', null, null, '6', 'A', '9000.00', '8900.00', '8800.00', '9500.00');

insert into DXSCH.SKU_OPTIONS (ID, SKU_ID, OPTION, VALUE) values ('1', '2', 'UQ', '12:U');
insert into DXSCH.SKU_OPTIONS (ID, SKU_ID, OPTION, VALUE) values ('2', '2', 'SZ', 'XXL');
insert into DXSCH.SKU_OPTIONS (ID, SKU_ID, OPTION, VALUE) values ('3', '3', 'UQ', '24:U');
insert into DXSCH.SKU_OPTIONS (ID, SKU_ID, OPTION, VALUE) values ('4', '3', 'SZ', 'L');

insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (1, 'Coca Cola', null, '1', '1', '1', 'A');
insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (2, 'Fanta', null, '1', '1', '4', 'A');
insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (3, 'Vimto', null, '1', '1', '5', 'A');
insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (4, 'Jumbo', null, '1', '2', '6', 'A');
insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (5, 'La Vache Qui Rit', null, '1', '3', '7', 'A');
insert into DXSCH.PRODUCT (ID, NAME, DESCRIPTION, B_UNIT, CATEGORY, DEFAULT_SKU, STATUS) values (6, 'Poulet Entier', null, '1', '4', '8', 'A');

insert into DXSCH.UZER (ID, FIRST_NAME, LAST_NAME, PASSWORD, ORGANIZATION, USER_NAME, CREATION_DATE, ACCOUNT_TYPE)  values (1, 'Abdoulaye', 'Diallo', 'password',  '1', 'dialla1', CURRENT_TIMESTAMP(), 'P');
insert into DXSCH.UZER (ID, FIRST_NAME, LAST_NAME, PASSWORD, ORGANIZATION, USER_NAME, CREATION_DATE, ACCOUNT_TYPE)  values (2, 'Lamine', 'Diallo', 'password', '1', 'dialla2', CURRENT_TIMESTAMP(), 'P');
insert into DXSCH.UZER (ID, FIRST_NAME, LAST_NAME, PASSWORD, ORGANIZATION, USER_NAME, CREATION_DATE, ACCOUNT_TYPE)  values (3, 'Sellou', 'Diallo', 'password',  '1', 'dialla3', CURRENT_TIMESTAMP(), 'P');
insert into DXSCH.UZER (ID, FIRST_NAME, LAST_NAME, PASSWORD, ORGANIZATION, USER_NAME, CREATION_DATE, ACCOUNT_TYPE)  values (4, 'Kaoussou', 'Diallo', 'password',  '1', 'dialla4', CURRENT_TIMESTAMP(), 'P');
insert into DXSCH.UZER (ID, BUSINESS_NAME, ORGANIZATION, CREATION_DATE, ACCOUNT_TYPE)  values (5, 'Societe Mabacke & Freres', '1', CURRENT_TIMESTAMP(), 'P');

insert into DXSCH.ROLE (ID, NAME, NOTATION) values (1, 'ADMIN', 'Administrateur');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (2, 'PUMPIST', 'Pompist');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (3, 'CLIENT', 'Client');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (4, 'CUSTOMER', 'Customer');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (5, 'MANAGER', 'Manager');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (6, 'GERANT', 'Gerant');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (7, 'CLERK', 'Caissier');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (8, 'BAYWASH', 'Laveur');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (9, 'BAYOIL', 'Graissuer');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (10, 'SUPERVISOR', 'Chef de Piste');
insert into DXSCH.ROLE (ID, NAME, NOTATION) values (11, 'CREDIT', 'Client a Credit');

insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(1, 1);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(1, 2);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(1, 5);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(1, 10);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(2, 2);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(3, 3);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(4, 8);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(4, 9);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(5, 3);
insert into DXSCH.USER_ROLE(USER_ID, ROLE_ID) values(5, 11);