/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.CreditLine;
import com.jalarbee.mango.domain.CreditOder;
import com.jalarbee.mango.domain.CreditTimeline;
import com.jalarbee.mango.domain.LineItem;
import com.jalarbee.mango.domain.User;
import java.util.Date;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.springframework.test.annotation.Rollback;
/**
 *
 * @author abdoul
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServiceConfiguration.class)
@ActiveProfiles(profiles = {"integration"})
public class CreditServiceTestIT {
    
    @Autowired private CreditService creditService;
    @Autowired private UserService userService;
    @Autowired private CatalogService catalogService;
    
    @Test
    @Transactional
    @Rollback(false)
    public void testNewCreditLine() {
        Date date = new Date();
        CreditLine line = new CreditLine();
        User user = userService.findOne(1L);
        CreditOder creditOrder = new CreditOder(date, line);
        LineItem item = new LineItem();
        item.setSku(catalogService.findSku(1L));
        item.setCreditOder(creditOrder);
        creditOrder.getProductItems().add(item);
        CreditTimeline timeline = new CreditTimeline();
        line.setCreditTimeline(timeline);
        line.setClient(user);
        line.getOrders().add(creditOrder);
        line = creditService.create(line);
        
        assertThat(line, notNullValue());
        assertThat(line.getCreditTimeline().getEvents(), hasSize(1));
    }
    
}
