/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jalarbee.mango.service;

import com.jalarbee.mango.domain.User;
import java.util.Iterator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 *
 * @author abdoul
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServiceConfiguration.class)
@ActiveProfiles(profiles = {"integration"})
public class CustomerServiceIT {
    
    @Autowired private CustomerService customerService;
    
    @Test
    public void testGetAllCustomers() {
        Iterable<User> customers = customerService.getCutsomers();
        assertThat(null, !equals(customers));
        Iterator<User> iterator = customers.iterator();
        assertThat(true, is(iterator.hasNext()));
        User lamine = iterator.next();
        assertThat("Abdoulaye", equalTo(lamine.getFirstName()));
        assertThat(4, equalTo(lamine.getRoles().size()));
        assertThat(true, equalTo(lamine.getRolesHolder().hasRole("ADMIN")));
        assertThat(true, is(iterator.hasNext())); iterator.next();
        assertThat(true, is(iterator.hasNext())); iterator.next();
        assertThat(true, is(iterator.hasNext())); iterator.next();
        assertThat(false, is(iterator.hasNext()));
    }
}
